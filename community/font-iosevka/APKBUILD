# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=font-iosevka
pkgver=22.0.2
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/iosevka-aile.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
067f14f49d45eff6b8f4f83dd11992b1f3f7561ab12a750ffc53abc67b1a7a4ff5d0f2111967b26a3021d673b66983ab3bed31e44493c151314639176e398e8c  super-ttc-iosevka-22.0.2.zip
3359b8bcefee5d5664921c99f2c9efff5ead343dcc42e90fdfa3bded4b76a492d3c0d65c924083f14a1ed94766f743544f8d32126848b2ef8e50fd5072dd4688  super-ttc-iosevka-aile-22.0.2.zip
8bd044a1dbbbdec2f0bd5e7adf2bc2c89823bad89a34501cc318cde1f7d535e5738ab6a542028f553a8f103e85a49ce17434410915d2b22ee0cd7a8191fe3d8d  super-ttc-iosevka-slab-22.0.2.zip
55a375f1a644902dbc400e639561a08ae3b253253219a8f8f03f175ccb9391dc2af355ed954c9a34c6c44f78e73086f44175ee7662b1a3e4a7af11251e7ef2ec  super-ttc-iosevka-curly-22.0.2.zip
091d550258056f1bdca590653a7a3e79c135fc4a52ace0bc21bc747aeb728ead23a23db34696f6374d6abe616bf1623c7dda26f7169c5b6b0605870f45ab1621  super-ttc-iosevka-curly-slab-22.0.2.zip
"
