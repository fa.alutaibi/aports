# Contributor: Sean McAvoy <seanmcavoy@gmail.com>
# Maintainer: Sean McAvoy <seanmcavoy@gmail.com>
pkgname=ansible-core
pkgver=2.14.4
pkgrel=0
pkgdesc="core components of ansible: A configuration-management, deployment, task-execution, and multinode orchestration framework"
url="https://ansible.com"
options="!check" # for now
arch="noarch"
license="GPL-3.0-or-later"
depends="
	py3-cryptography
	py3-jinja2
	py3-packaging
	py3-paramiko
	py3-resolvelib
	py3-yaml
	python3
	"
makedepends="py3-setuptools"
subpackages="$pkgname-doc"
source="https://pypi.python.org/packages/source/a/ansible-core/ansible-core-$pkgver.tar.gz
	resolvelib.patch
	"

replaces="ansible-base"

build() {
	python3 setup.py build
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	mkdir -p "$pkgdir"/usr/share/doc/$pkgname/examples/
	cp -r examples/* \
	"$pkgdir"/usr/share/doc/$pkgname/examples/
	install -m644 README.rst "$pkgdir"/usr/share/doc/$pkgname

	mkdir -p "$pkgdir"/usr/share/man/
	local man
	for man in ./docs/man/man?/*.?; do
		install -Dm644 "$man" \
			"$pkgdir"/usr/share/man/man${man##*.}/${man##*/}
	done
}

sha512sums="
86164dfded15232174e4f11140a71b91ef5b0d93b40a2df9588930b4b955f20feb419258c2bbf9d6735d298ec626c88c5e9b1c933a51e3273a28b85fac7a3762  ansible-core-2.14.4.tar.gz
2debd5854b096f951aa00eb77ffe2f08f515944d7d6de9bf34a6cc0a624e6ae9fa40ab2c4b62f3cf8dfa1f18831e733eb3c494a6b101a62cea2a0f433168a304  resolvelib.patch
"
