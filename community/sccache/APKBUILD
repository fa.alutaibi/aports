# Contributor: psykose <alice@ayaya.dev>
# Maintainer: omni <omni+alpine@hack.org>
pkgname=sccache
pkgver=0.4.1
pkgrel=0
pkgdesc="shared compilation cache for C/C++ and Rust"
url="https://github.com/mozilla/sccache/"
# s390x & riscv64: limited by cargo
# ppc64le: not supported by ring crate
# armhf: sigbus
arch="all !s390x !ppc64le !armhf !riscv64"
license="Apache-2.0"
makedepends="cargo openssl-dev>3"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/mozilla/sccache/archive/v$pkgver.tar.gz
	test_musl_ldd_parse.patch
	"

case "$CARCH" in
x86_64)
	# only x86_64 supports sccache-dist
	subpackages="$subpackages $pkgname-dist"
	_features="all,dist-server"
	;;
*)
	_features="all"
	;;
esac

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo build --release --frozen --features $_features
}

check() {
	cargo test --frozen
}

package() {
	install -Dm0755 target/release/sccache -t "$pkgdir"/usr/bin

	case "$CARCH" in
	x86_64)
		install -Dm0755 target/release/sccache-dist \
			-t "$pkgdir"/usr/bin
		;;
	esac

	install -Dm0644 -t "$pkgdir"/usr/share/doc/"$pkgname" \
		docs/Distributed.md \
		docs/DistributedQuickstart.md \
		docs/Jenkins.md \
		docs/Rust.md
}

dist() {
	pkgdesc="$pkgdesc (dist server)"

	amove usr/bin/sccache-dist
}

sha512sums="
94b5d2b7fadeca324e91e1e395225d0765ae35978f553a2f0f6f4fb7c7410673153a1dd1756fb7b2e44e7e383e6d2869976b744a07a11b16f866d70645d59330  sccache-0.4.1.tar.gz
d8e544829df05fd374518f7daa3fc09b129877288594aafedfa43aeaa358779475c763b58801a7591bc4dd5b555ec4326957df18628e661798c73c0dd6da3bec  test_musl_ldd_parse.patch
"
