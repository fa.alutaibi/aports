# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=shotwell
pkgver=0.31.90
pkgrel=0
pkgdesc="A digital photo organizer designed for the GNOME desktop environment"
url="https://wiki.gnome.org/Apps/Shotwell"
arch="all"
license="CC-BY-SA-3.0 AND LGPL-2.1-or-later"
subpackages="$pkgname-lang $pkgname-doc"
makedepends="
	dbus-glib-dev
	gcr-dev
	gexiv2-dev
	gst-plugins-base-dev
	gtk+3.0-dev
	itstool
	json-glib-dev
	libexif-dev
	libgdata-dev
	libgee-dev
	libgphoto2-dev
	libgudev-dev
	libportal-dev
	libraw-dev
	libsecret-dev
	meson
	sqlite-dev
	vala
	webkit2gtk-4.1-dev
	"
checkdepends="desktop-file-utils appstream-glib"
source="https://download.gnome.org/sources/shotwell/${pkgver%.*}/shotwell-$pkgver.tar.xz"

build() {
	abuild-meson \
		-Db_lto=true \
		-Dinstall_apport_hook=false \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
8a453ef506285be98ee6dc2b90599db3651984c1d635dc66dcb6ab0df5c123089e0467963b1284c75125026683ea37f9e71b582e0f3f0de8832722519bfe88fb  shotwell-0.31.90.tar.xz
"
