# Contributor: Stuart Cardall <developer@it-offshore.co.uk>
# Maintainer: Stuart Cardall <developer@it-offshore.co.uk>
pkgname=py3-tox
_pkgname=${pkgname#py3-*}
pkgver=4.4.12
pkgrel=1
pkgdesc="virtualenv management and test command line tool"
options="!check" #  Requires community/py3-pathlib2, and unpackaged flaky
url="https://tox.readthedocs.org/"
arch="noarch"
license="MIT"
depends="
	py3-cachetools
	py3-chardet
	py3-colorama
	py3-filelock
	py3-packaging
	py3-platformdirs
	py3-pluggy
	py3-pyproject-api
	py3-virtualenv
	python3
	"
makedepends="
	py3-gpep517
	py3-hatch-vcs
	py3-hatchling
	"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-tox" # Backwards compatibility
provides="py-tox=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 setup.py test
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
896ecef178b0625246ffe4ab3a9a0c03650eb78633fa92706e93475d6589ac29d65bbf91b9ecfacfa8154739eefd463aeb1252be68998cdffddc270a4f89edd0  tox-4.4.12.tar.gz
"
