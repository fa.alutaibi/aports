# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-zeroconf
pkgver=0.56.0
pkgrel=1
pkgdesc="Python implementation of multicast DNS service discovery"
url="https://github.com/jstasiak/python-zeroconf"
arch="all"
license="LGPL-2.0-or-later"
replaces="py-zeroconf" # for backwards compatibility
provides="py-zeroconf=$pkgver-r$pkgrel" # for backwards compatibility
depends="python3 py3-ifaddr"
makedepends="
	cython
	py3-gpep517
	py3-poetry-core
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="py3-pytest py3-pytest-cov py3-pytest-asyncio"
subpackages="$pkgname-pyc"
source="python-zeroconf-$pkgver.tar.gz::https://github.com/jstasiak/python-zeroconf/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir"/python-zeroconf-$pkgver

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
ead036e328ebb6349e50c274293e0888da9d0aafd2d7dd6e4c9a02616432636647cd5056c887439994d14833d763996a8d510f4b6d2b3789c4f3f9d8ef06743d  python-zeroconf-0.56.0.tar.gz
"
