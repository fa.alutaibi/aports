# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=esbuild
pkgver=0.17.17
pkgrel=0
pkgdesc="Extremely fast JavaScript bundler and minifier"
url="https://esbuild.github.io/"
license="MIT"
arch="all"
makedepends="go nodejs"
source="https://github.com/evanw/esbuild/archive/v$pkgver/esbuild-$pkgver.tar.gz"
options="net" # fetch dependencies

export GOPATH="$srcdir"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build \
		-ldflags="-X main.version=$pkgver" \
		-v ./cmd/esbuild

	node scripts/esbuild.js npm/esbuild/package.json --version
	node scripts/esbuild.js ./esbuild --neutral

	# binary path override
	sed -i '1s#^#var ESBUILD_BINARY_PATH = "/usr/bin/esbuild";\n#' \
		npm/esbuild/lib/main.js
}

check() {
	go test ./...
}

package() {
	install -Dm755 esbuild "$pkgdir"/usr/bin/esbuild

	local destdir=/usr/lib/node_modules/esbuild

	install -d \
		"$pkgdir"/$destdir/bin \
		"$pkgdir"/$destdir/lib

	install -Dm644 -t "$pkgdir"/$destdir npm/esbuild/package.json
	install -Dm644 -t "$pkgdir"/$destdir/lib npm/esbuild/lib/*
	ln -s /usr/bin/esbuild "$pkgdir"/$destdir/bin/esbuild
}

sha512sums="
2ed073f70b4e77236d921925bd0ed529ee3022ad4ec3a3cc7effc715ad4ebf058548d844c744d3bdb70a84b245ce3ddf97ad6a7054a04239b1300a04940b1792  esbuild-0.17.17.tar.gz
"
