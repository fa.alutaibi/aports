# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-pdm-backend
pkgver=2.0.5
pkgrel=1
pkgdesc="Build backend used by PDM that supports latest packaging standards"
url="https://pdm-backend.fming.dev/"
arch="noarch"
license="MIT"
depends="
	py3-cerberus
	py3-editables
	py3-license-expression
	py3-packaging
	py3-pyproject-metadata
	py3-tomli
	py3-tomli-w
	py3-validate-pyproject
	python3
	"
makedepends="py3-gpep517 py3-installer"
checkdepends="py3-pytest py3-setuptools python3-dev"
source="https://github.com/pdm-project/pdm-backend/archive/refs/tags/$pkgver/py3-pdm-backend-$pkgver.tar.gz
	unvendor.patch
	"
builddir="$srcdir/pdm-backend-$pkgver"

build() {
	PDM_BUILD_SCM_VERSION=$pkgver \
	gpep517 build-wheel	\
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
	rm -rf "$pkgdir"/usr/lib/python*/site-packages/pdm/backend/_vendor
}

sha512sums="
a219c7d05162127fa029f7386105a83437f396089933e948f2f5779aed87a6800521dd4735be9a3abf3374aabfbcae351975143b6472aa4a431f827055f0e244  py3-pdm-backend-2.0.5.tar.gz
cfa87d830b1d3e7139234a97f6cc808c55cb2a8ff219316a1b6d2a080792a2bb919ac5738cf2fc22b2322f0fd9d058b5c8082dcbc59dd3effd0c2a52b9fe9779  unvendor.patch
"
