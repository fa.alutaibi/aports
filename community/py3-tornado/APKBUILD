# Maintainer:
pkgname=py3-tornado
_pkgname=tornado
pkgver=6.3
pkgrel=1
pkgdesc="Python3 web framework and asynchronous networking library"
options="!check" # 3 Tests fail by failure to resolve 'localhost'
url="http://www.tornadoweb.org/"
arch="all"
license="Apache-2.0"
depends="python3"
makedepends="python3-dev py3-setuptools"
checkdepends="py3-curl py3-mock py3-twisted"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-tornado" # Backwards compatibility
provides="py-tornado=$pkgver-r$pkgrel" # Backwards compatibility

export TORNADO_EXTENSION=1

build() {
	python3 setup.py build
}

check() {
	export PYTHONPATH="$(echo $PWD/build/lib.*)"
	cd build
	python3 -m tornado.test.runtests
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
	rm -rf "$pkgdir"/usr/lib/python3*/site-packages/$_pkgname/test
}

sha512sums="
f25bbf1e489bb53ba5ed502a9241b3629a66a8bee3ba54d8bd96946e478ea06dfdf70e6f937da25c8240cb16cc5c9ff3011415503c7d03e2b1eeb2f7567d8209  tornado-6.3.tar.gz
"
