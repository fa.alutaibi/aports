# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
pkgname=py3-imageio
pkgver=2.27.0
pkgrel=0
pkgdesc="Python library that provides an easy interface to read and write a wide range of image data"
url="https://github.com/imageio/imageio"
license="BSD-2-Clause"
# ppc64le: test failures
# s390x: freeimage
arch="noarch !ppc64le !s390x"
depends="python3 py3-numpy py3-pillow freeimage"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-psutil py3-imageio-ffmpeg py3-fsspec"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/i/imageio/imageio-$pkgver.tar.gz"
builddir="$srcdir/imageio-$pkgver"
options="!check" # intentionally fail without internet(?), todo

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$PWD"/build/lib IMAGEIO_NO_INTERNET=1 pytest -v
}

package() {
	python3 setup.py install --root="$pkgdir"

	# remove unneeded binaries
	# shellcheck disable=2115
	rm -r "$pkgdir"/usr/bin
}

sha512sums="
21a634e1d3858ec6448ae223a5dadb48e0134e8df97019305a40cd74b99fa379ff6886c66d41dc95f384abc0abd79f96b455ac1519dd134a1e06946ca35bc57c  py3-imageio-2.27.0.tar.gz
"
