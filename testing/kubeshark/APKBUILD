# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=kubeshark
pkgver=39.6
pkgrel=0
pkgdesc="API traffic viewer for Kubernetes"
url="https://kubeshark.co/"
arch="all"
license="Apache-2.0"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/kubeshark/kubeshark/archive/refs/tags/$pkgver.tar.gz"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local _goldflags="
	-X github.com/kubeshark/kubeshark/misc.GitCommitHash=AlpineLinux
	-X github.com/kubeshark/kubeshark/misc.Branch=main
	-X github.com/kubeshark/kubeshark/misc.BuildTimestamp=$(date -u "+%s" ${SOURCE_DATE_EPOCH:+-d @$SOURCE_DATE_EPOCH})
	-X github.com/kubeshark/kubeshark/misc.Platform=linux_$(go env GOARCH)
	-X github.com/kubeshark/kubeshark/misc.Ver=$pkgver
	"
	go build -v -o bin/$pkgname -ldflags "$_goldflags" kubeshark.go

	for shell in bash fish zsh; do
		./bin/$pkgname completion $shell > $pkgname.$shell
	done
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/$pkgname -t "$pkgdir"/usr/bin/

	install -Dm644 $pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
cc692f7755cbbe7d0626b629a5fc41228ceb3363b080e6c8dcac624c1b13c0094ceecdf079038933b0d73ef453fd1aa48190e1b372669d50630c54a2f6c4f7e2  kubeshark-39.6.tar.gz
"
